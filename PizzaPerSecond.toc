## Interface: 90001

## Title: Pizza Per Second
## Notes: An addon that calculates your pizza per second and reports it to chats.
## Author: Deathbypizza
## Version: v0.6
## SavedVariables: PPSSillinessEnabled
## SavedVariablesPerCharacter: PPSUnit

utilities.lua
ppsFunctions.lua
main.lua
