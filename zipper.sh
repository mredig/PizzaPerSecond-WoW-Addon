#/bin/sh

ADDON_DIR_NAME="PizzaPerSecond"
pwd
mkdir "$ADDON_DIR_NAME"
cp -v *.lua "$ADDON_DIR_NAME"/
cp -v *.toc "$ADDON_DIR_NAME"/
cp -v *.xml "$ADDON_DIR_NAME"/
cp -v *.txt "$ADDON_DIR_NAME"/
cp -v *.md "$ADDON_DIR_NAME"/
zip -r "$ADDON_DIR_NAME".zip "$ADDON_DIR_NAME"
rm -rf "$ADDON_DIR_NAME"/
